package org.paumard.tpjpa;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import org.paumard.tpjpa.model.User;
import org.paumard.tpjpa.model.UserFactory;
import org.paumard.tpjpa.util.Civility;

public class Main {

	public static void main(String[] args) {

		UserFactory factory=new UserFactory();
		
		EntityManager entityManager = factory.getEntityManager();
		
		Calendar calendar = new GregorianCalendar(1980, 5, 20);
		Date date = calendar.getTime();
		
		User user1 = new User("Kylian","Mbapp�", 19, date, Civility.MR);
		User user2 = new User("Paul","Ndiaye", 23, date, Civility.MR);
		User user3 = new User("Abdou","Gueye",22,date, Civility.MR);
		
		System.out.println("User 1 = " + user1);
		System.out.println("User 2 = " + user2);
		System.out.println("User 3 = " + user3);
		
		entityManager.getTransaction().begin();
		
		entityManager.persist(user1);
		entityManager.persist(user2);
		entityManager.persist(user3);
		
		entityManager.getTransaction().commit();
		User find=entityManager.find(User.class, 2L);
		if(find==null) {
			System.out.println("L'objet ne se trouve dans la base de donn�es");
		}
		else {
			System.out.println("L'utilisateur trouv�:"+find);
		}
		
		
        entityManager.getTransaction().begin();
		
		user1.setFirstName("Imam");
		//mis � jour
		int val;
		if((val=factory.delete(3L))==1)
			System.out.println("supprim� !!!");
		else
			System.out.println("nope");
		entityManager.getTransaction().commit();
		 
		System.out.println("User 1 = " + user1);
		System.out.println("User 2 = " + user2);
	}
}








