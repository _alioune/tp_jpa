package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Departement
 *
 */
@Table(name="departements")
@Entity
public class Departement implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=50)
	private String nom;
	@Column(length=50)
	private String code;
	@OneToMany(mappedBy="dpt")
	private Collection<Commune> communes;

	public Departement() {
		super();
	}
	public static int getNumberOfTowns(){
		UserFactory factory=new UserFactory();
		EntityManager em = factory.getEntityManager();
		Query query = em.createQuery(
			    "select c from Commune c") ;
			return query.getResultList().size();
	}
	public static Maire getOldestMayor() {
		UserFactory factory=new UserFactory();
		EntityManager em = factory.getEntityManager();
		Query query = em.createQuery(
			    "select m from Maire m orderBy m.dateOfBirth desc ") ;
		return (Maire)query.getResultList().get(0);
	}
	public static double getWomanRatio() {
		UserFactory factory=new UserFactory();
		EntityManager em = factory.getEntityManager();
		Query query = em.createQuery(
			    "select m from Maire m where m.civilite='Mme'");
		return 100*query.getResultList().size()/getNumberOfTowns();
	}
   
}
