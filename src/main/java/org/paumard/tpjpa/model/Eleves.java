package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Eleves
 *
 */
@Entity(name="Eleves")
public class Eleves implements Serializable {

	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=30)
	private String nom;
	@ManyToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<Instruments> instruments=new ArrayList<>();

	public void setInstruments(Collection<Instruments> instruments) {
		this.instruments = instruments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	private int age;
	public Eleves(String nom, int age) {
		this.nom=nom;
		this.age=age;
	}
	
	

	public Collection<Instruments> getInstruments() {
		return instruments;
	}

	public void addInstruments(Instruments inst) {
		this.instruments.add(inst);
	}
	public static List<Eleves> listEleves(EntityManager em){
		// construction d'un objet Query	
				Query query = em.createQuery(
				    "select DISTINCT e from Eleves e INNER JOIN e.instruments instru order by e.nom,instru.nom") ;
				return query.getResultList();
	}



	public Eleves() {
		super();
	}

	@Override
	public String toString() {
		return "Eleves [id=" + id + ", nom=" + nom + ", instruments=" + instruments + ", age=" + age + "]";
	}
	
   
}
