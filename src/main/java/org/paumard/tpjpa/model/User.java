package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.paumard.tpjpa.util.Civility;

@Entity @Table(name="t_user", uniqueConstraints= {@UniqueConstraint(columnNames= {"firstName","lastName"})})
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=40)
	private String firstName;
	@Column(length=40)
	private String lastName;
	private int age;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	@Enumerated(EnumType.STRING)
	private Civility civility;

	public User() {
	}
	
	public User(String firstName,String lastName, int age, Date dateOfBirth, Civility civility) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.age=age;
		this.dateOfBirth = dateOfBirth;
		this.civility = civility;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Civility getCivility() {
		return civility;
	}

	public void setCivility(Civility civility) {
		this.civility = civility;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + " lastName=" + lastName +", age=" + age + ", dateOfBirth=" + dateOfBirth + ", civility="
				+ civility + "]";
	}

}
