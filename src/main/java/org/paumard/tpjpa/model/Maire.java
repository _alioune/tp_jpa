package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Maire
 *
 */
@Table(name="maires")
@Entity

public class Maire implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=50)
	private String nom;
	@Column(length=50)
	private String prenom;
	@Column(length=5)
	private String civilite;
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	@OneToOne(mappedBy="maire")  // r�f�rence la relation dans la classe Commune
    private Commune commune ;

	public Maire() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "Maire [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civilite=" + civilite + ", dateOfBirth="
				+ dateOfBirth + "]";
	}
	
   
}
