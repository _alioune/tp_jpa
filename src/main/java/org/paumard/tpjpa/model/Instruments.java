package org.paumard.tpjpa.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Instruments
 *
 */
@Entity(name="Instruments")

public class Instruments implements Serializable {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=30)
	private String nom;
	private int prix;
	private int prix_cours;
	private char location;
	@OneToOne(mappedBy="instrument")
	private Professeurs prof;
	@ManyToOne(fetch=FetchType.LAZY)
	private Eleves eleve;
	public Instruments(String nom,int prix, int prix_cours, char location) {
		this.nom=nom;
		this.prix=prix;
		this.prix_cours=prix_cours;
		this.location=location;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public int getPrix_cours() {
		return prix_cours;
	}

	public void setPrix_cours(int prix_cours) {
		this.prix_cours = prix_cours;
	}

	public char getLocation() {
		return location;
	}

	public void setLocation(char location) {
		this.location = location;
	}

	
	private static final long serialVersionUID = 1L;

	public Instruments() {
		super();
	}

	@Override
	public String toString() {
		return "Instruments [id=" + id + ", nom=" + nom + ", prix=" + prix + ", prix_cours=" + prix_cours
				+ ", location=" + location + "]";
	}
	
   
}
