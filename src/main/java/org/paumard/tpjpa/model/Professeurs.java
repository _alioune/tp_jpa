package org.paumard.tpjpa.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Professeurs
 *
 */
@Entity(name="Professeurs")

public class Professeurs implements Serializable {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	@Column(length=30)
	private String nom;
	private int age;
	@OneToOne
	private Instruments instrument;
    
	
	private static final long serialVersionUID = 1L;

	public Professeurs() {
		super();
	}
    public Professeurs(String nom,int age,Instruments inst) {
    	this.age=age;
    	this.nom=nom;
    	instrument=inst;
    }
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public static List<Professeurs> listProf(EntityManager em) {
		// construction d'un objet Query	
		Query query = em.createQuery(
		    "select prof from Professeurs prof ORDER BY prof.nom") ;
		return query.getResultList();

	}

    
	@Override
	public String toString() {
		return "Professeurs [id=" + id + ", age=" + age + ", " + ", inst=" + instrument + "]";
	}
	
   
}
