package org.paumard.tpjpa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;

import org.paumard.tpjpa.model.Eleves;
import org.paumard.tpjpa.model.Instruments;
import org.paumard.tpjpa.model.Professeurs;
import org.paumard.tpjpa.model.User;
import org.paumard.tpjpa.model.UserFactory;
import org.paumard.tpjpa.util.Civility;

public class MainExo2 {
	public static void main(String[] args) {

		UserFactory factory=new UserFactory();
		EntityManager entityManager = factory.getEntityManager();
		
		
		Instruments ins1=new Instruments("Basson",400,20,'N');  // rien ne nous emp�cher d'utiliser la r�flection pour cr��r les �l�ments du fichier
		Instruments ins2=new Instruments("Clarinette",350,20,'N'); // comme le fichier n'est pas long... on le fait � la main
		Instruments ins3=new Instruments("Clavecin",5000,30,'O');
		Instruments ins4=new Instruments("Piano",3000,20,'O');
		Instruments ins5=new Instruments("Fl�te",200,15,'N');
		Instruments ins6=new Instruments("Hautbois",300,20,'N');

		Eleves e1=new Eleves("Albert", 35);
		Eleves e2=new Eleves("Andr�", 32);
		Eleves e3=new Eleves("Antoine",18);
		Eleves e4=new Eleves("Aude",22);
		
		System.out.println("Instrument 1 = " + ins1);
		System.out.println("Inst2 = " + ins2);
		System.out.println("Instrument 3 = " + ins3);
		System.out.println("Inst4 = " + ins4);
		System.out.println("---------------------------");
		
		Professeurs p1=new Professeurs("Aude",22,ins5);
		Professeurs p2=new Professeurs("V�ronique",29,ins4);
				
		entityManager.getTransaction().begin();
		
		entityManager.persist(ins1);
		entityManager.persist(ins2);	
		entityManager.persist(ins3);
		entityManager.persist(ins4);
		entityManager.persist(ins5);
		entityManager.persist(ins6);
		entityManager.getTransaction().commit();
		
		entityManager.getTransaction().begin();
		e1.getInstruments().add(ins3);
		e1.getInstruments().add(ins4);
		e2.getInstruments().add(ins2);
		e2.getInstruments().add(ins6);
		e2.getInstruments().add(ins5);
		e3.getInstruments().add(ins4);
		e4.getInstruments().add(ins6);
		e4.getInstruments().add(ins5);
		entityManager.persist(e1);
		entityManager.persist(e2);
		entityManager.persist(e3);
		entityManager.persist(e4);
		entityManager.getTransaction().commit();
		
		entityManager.getTransaction().begin();
		entityManager.persist(p1);
		entityManager.persist(p2);
		entityManager.getTransaction().commit();
		
		
		
		Instruments find=entityManager.find(Instruments.class, 1L);
		if(find==null) {
			System.out.println("L'objet ne se trouve dans la base de donn�es");
		}
		else {
			System.out.println("L'utilisateur trouv�:"+find);
		}
		System.out.println("---------------------------");
		for (Instruments ins : e1.getInstruments()) {
			System.out.println(ins);
		}
		
		System.out.println("---------------------------");
		//Ajoutons les entit�s
		Instruments ins7=new Instruments("Saxophone",1000,15,'O');
		entityManager.getTransaction().begin();
		entityManager.persist(ins7);
		entityManager.getTransaction().commit();
		Professeurs p3=new Professeurs("Victor",38,ins7);
		
		Eleves adrian=new Eleves("Adrian",19);
		entityManager.getTransaction().begin();
		adrian.getInstruments().add(ins7);
		adrian.getInstruments().add(ins2);
		entityManager.persist(adrian);
		entityManager.getTransaction().commit();
		entityManager.getTransaction().begin();
		entityManager.persist(p3);
		entityManager.getTransaction().commit();
		List<Professeurs> profs=Professeurs.listProf(entityManager);
		for(Professeurs p: profs) {
			System.out.println(p);
		}
		System.out.println("---------------------------");
		List<Eleves> eleves=Eleves.listEleves(entityManager);
		for(Eleves e: eleves) {
			System.out.println(e);
		}
		
		entityManager.close();

	}

}
