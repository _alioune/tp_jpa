package org.paumard.tpjpa.model;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

public class UserFactory {
	private EntityManagerFactory entityManagerFactory = 
			Persistence.createEntityManagerFactory("jpa-test");
	
	private static EntityManager entityManager;
	public UserFactory() {
		entityManager= entityManagerFactory.createEntityManager();
	}
	

	public EntityManager getEntityManager() {
		return entityManager;
	}
	public User find(long id) {
		return entityManager.find(User.class, id);
	}
	@Transactional
	public int delete(long id) {
       int isSuccessful=entityManager.createQuery("delete from User u where u.id=:id")
       .setParameter("id", id)
       .executeUpdate();
		return isSuccessful;
		
        
	}
	

}
