## TP JPA

### Cr�ation d'une base MySQL

- Cr�er un sch�ma `tp_jpa` dans votre serveur MySQL
- Cr�er un utilisateur `jpa_user` avec un mot de passe `jpa`
- Donner les droits sur le sch�ma `tp_jpa` � l'utilisateur `tp_jpa`

### Cr�ation du projet TP_JPA

- Cr�er un projet Maven `TP_JPA`
- Configurer le projet en Java 8 dans le `pom.xml`
- Ajouter les d�pendances suivantes : MySQL Connector, Java EE, Hibernate
- Cr�er un r�pertoire `META-INF` dans le r�pertoire `src/main/resources`
- Cr�er un fichier `persistence.xml` dans ce r�pertoire   
### exercice 5  
3.a relation Commune d�partement: relation p:1 biderectionnelle  
4. Oui on peut cr�er une �num�ration puisqu'il n'y a que M et Mme


